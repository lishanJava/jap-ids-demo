#!/bin/bash

help(){
  echo "--------------------------------------------------------------------------"
  echo ""
  echo "usage: ./docker-shell.sh"
  echo ""
  echo "--------------------------------------------------------------------------"
}

help

# 打包, -q: 控制 Maven 的日志级别,仅仅显示错误
echo ">> Start packaging the project..."
mvn -q clean package -Dmaven.test.skip=true
echo ">> Project packaged successfully!"
echo ""

# 构建镜像
echo ">> Start building the docker image..."
docker build -t jap-ids-demo .
echo ">> The docker image is successfully built!"
echo ""

# 运行容器
echo ">> Start the docker container..."
docker run --name jap-ids-demo -p 8081:8081 -d jap-ids-demo
echo ">> The docker container started successfully"
echo ""

# 启动浏览器，输入：http://localhost:8081
echo ">> Please enter in the browser: http://localhost:8081"
