# 环境版本
FROM fujieid/java:8-jre

MAINTAINER yadong.zhang0415@gmail.com

ENV TZ=PRC

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# 指定容器端口
EXPOSE 8081

# 添加本地 JAR 到容器内
ADD target/jap-ids-demo-*.jar app.jar

# 容器启动后执行的命令
ENTRYPOINT ["java", "-jar","app.jar" , "-Djava.security.egd=file:/dev/./urandom"]
